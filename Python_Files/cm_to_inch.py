#Centimeters to Inches conversion project
print ('\nCentimeters to Inches converter\n')
num =float( input ('type a value in centimeters: '))
if num < 0:
    print('\nError, length cannot be measured in negative integers\n')
else:
    print ('\nThe length in Inches is:', num/2.54, '\n')
