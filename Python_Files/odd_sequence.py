print('\nThis is a program that prints odd numbers in a sequence')
n = int(input('\nEnter the number of terms that needs to be printed: '))
print('\nOdd terms in descending order:\n')
for i in range (n -1, -1, -1):
    a = 1 + i*2
    print(a)
print('\n')
