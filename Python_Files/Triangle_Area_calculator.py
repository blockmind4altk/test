import math

print ('\nArea of Triangle Calculator:')

s1 = float(input("\nEnter the value of first side: "))
s2 = float(input("Enter the value of second side: "))
s3 = float(input("Enter the value of third side: "))

#calculates semi-perimeter
sp = (s1 + s2 + s3)/2

#calculates area of the triangle using herons formula
ar = math.sqrt(sp*(sp-s1)*(sp-s2)*(sp-s3))

#prints the area of the triangle
print("\nThe area of the triangle is:", ar,"units")
