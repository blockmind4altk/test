#This is python program for producing Fibonacci Sequence
print('\nThis program prints the Fibonacci Sequence:\n')

n = int(input("Please enter the number of terms: "))

a,b = 0,1
num  = 0

if n <= 0:
    print("Error, Enter a positive integer")
else:
    print('\nFibonacci Sequence:')
    for i in range (n):
        print (a)
        sum = a+b
        a = b
        b = sum 
        num = num+1
